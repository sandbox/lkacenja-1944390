<?php
/**
 * Theme includes
 */
$theme_path = drupal_get_path('theme', 'minim');
require_once($theme_path . '/includes/base.inc');
require_once($theme_path . '/includes/views.inc');
require_once($theme_path . '/includes/components.inc');
